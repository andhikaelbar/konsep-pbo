# Konsep PBO
PBO adalah konsep pembuatan program dengan memecah permasalahan program dengan menggunakan objek. Dalam PBO fungsi dan variabel harus dibungkus di dalam objek atau class yang dapat saling berinteraksi.

## Perbedaan dan Keunggulan PBO dibanding jenis Pemrograman lainnya
- Berbeda dengan jenis pemrograman lainnya yang tidak memiliki class, PBO memiliki class untuk membungkus data2 nya. 

- PBO juga lebih cocok digunakan untuk program berskala kompleks karena lebih terstruktur sehingga memudahkan dalam proses pembuatan aplikasi.

## Pertanyaan
Sebutkan bahasa pemrograman apa saja yg dapat menggunakan konsep PBO!

## License
Made by Kelompok 2
- Andhika Elbar Fahreza
- Dimas Kristio Saputro
